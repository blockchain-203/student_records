//SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract SubjectList {
    //Store Subjects Count
    uint public subjectsCount = 0;
    //Model a Subject
    struct Subject {
        uint _id;
        string code;
        string name;
        bool retired;
    }
    //store Subjects
    mapping(uint => Subject) public subjects;
    //Events
    event createSubjectEvent(
        uint _id,
        string indexed code,
        string name,
        bool retired
    );
    event markRetiredEvent(uint indexed _id);

    //Constructor for students
    constructor() {
        //createSubject("CSC101", "Front End Web Development I");
    }

    function createSubject(
        string memory _code,
        string memory _name
    ) public returns (Subject memory) {
        subjectsCount++;
        subjects[subjectsCount] = Subject(subjectsCount, _code, _name, false);
        //trigger create event
        emit createSubjectEvent(subjectsCount, _code, _name, false);
        return subjects[subjectsCount];
    }

    function markRetired(uint _id) public returns (Subject memory) {
        subjects[_id].retired = true;
        //trigger create event
        emit markRetiredEvent(_id);
        return subjects[_id];
    }

    //Fetch Subject
    function findSubject(uint _id) public view returns (Subject memory) {
        return subjects[_id];
    }
}
