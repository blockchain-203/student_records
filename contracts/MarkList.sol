// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract MarkList {
    uint public marksCount = 0;
    enum Grades {
        A,
        B,
        C,
        D,
        Fail
    }
    struct Marks {
        uint cid;
        string code;
        Grades grades;
    }
    mapping(uint => mapping(string => Marks)) public marks;
    event addMarksEvent(uint indexed cid, string indexed code, Grades grades);

    // constructor() {
    //     // addMarks(1001, "CSC101",1);
    // }
    function addMarks(uint _cid, string memory _code, Grades _grade) public {
        require(bytes(marks[_cid][_code].code).length == 0, "marks not found!");
        marksCount++;
        marks[_cid][_code] = Marks(_cid, _code, _grade);
        emit addMarksEvent(_cid, _code, _grade);
    }

    function findMarks(
        uint _cid,
        string memory _code
    ) public view returns (Marks memory) {
        return marks[_cid][_code];
    }

    function updateMarks(
        uint _cid,
        string memory _code,
        Grades _newgrade
    ) public returns (Marks memory) {
        require(
            bytes(marks[_cid][_code].code).length != 0,
            "marks is not assigned!"
        );
        marks[_cid][_code].grades = _newgrade;
        emit updateMarksEvent(_cid, _code, _newgrade);
        return marks[_cid][_code];
    }

    event updateMarksEvent(
        uint indexed cid,
        string indexed code,
        Grades grades
    );
}
