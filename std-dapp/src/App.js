import React, { Component } from "react";
import Web3 from 'web3';
import './App.css';
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from "./abi/config_studentList";
import StudentList from "./components/StudentList";

class App extends Component {

  componentDidMount() {
    if (!window.ethereum) 
      throw new Error("No crypto wallet found. please install it");
    window.ethereum.send("eth_requestAccounts");
    this.loadBlockchainData()
  }

  async loadBlockchainData() {
    const web3 = new Web3(Web3.givenProvider || "http://localhost:7545") 
    const accounts = await web3.eth.getAccounts()
    this.setState({account: accounts[0]})
    //step 4: load all the list from the blockchain
    const studentList = new web3.eth.Contract(
      STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS
    )
    //keep the list in the current state
    this.setState({studentList})
    //get the records for all list in the blockhain
    const studentCount = await studentList.methods.studentsCount().call()
    //store this value in the current state as well
    this.setState({studentCount})
    //use an iteration to extract each record info and store them in an array
    this.state.students=[]
    for(var i = 1;i <= studentCount; i++){
      const student = await studentList.methods.students(i).call()
      this.setState({
        students: [...this.state.students,student ]
      })
    }
  }

  constructor(props) {
    super(props) 
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading: true,
      update:false,
      studentObj:{},

    }
    this.createStudent = this.createStudent.bind(this)
    this.markGraduated = this.markGraduated.bind(this)
  }
  createStudent(cid,name){
    this.setState({loading: true})
    this.state.studentList.methods.createStudent(cid,name)
    .send({from: this.state.account})
    .once('receipt', (receipt) =>{
      this.setState({ loading: false})
      this.loadBlockchainData()
    })
  }
  updateName(id, name){
    //alert(id+name)
    this.setState({update:true})
    this.state.studentList.methods.updateStudent(id,name)
    .send({from:this.state.account})
    .once('receipt',(receipt)=>{
      this.setState({loading:false})
      this.setState({update:false})
      this.loadBlockchainData()
    })
    this.setState({loading:false})
    this.setState({update:false})
    // alert(student._id)
  }
  markGraduated(cid) {
    this.setState({loading : true})
    this.state.studentList.methods.markGraduated(cid)
    .send({from:this.state.account})
    .once('receipt', (receipt) =>{
      this.setState({loading : false})
      this.loadBlockchainData()
    })
  }
  render() {
    return (
      <div className="container">
        <h1>Student Marks Management System</h1>
        <p>Your account: {this.state.account}</p>
        <StudentList
        createStudent = {this.createStudent}/>
        <ul id="studentList" className="list-unstyled">
          {
            //This get the each student from the studentList
            //and pass them into a function that display the 
            //details of the student
            this.state.students.map((student, key) =>{
              return(
                <li className="list-group-item checkbox" key={key}>
                  <span className="name alert">{student._id}. {student.cid} {student.name}</span>
                  <input className="form-check-input" type="checkbox" name={student._id} defaultChecked={student.graduated}
                   disabled={student.graduated} ref={(input) => {
                    this.checkbox = input
                   }}
                   onClick={(event) =>{
                    this.markGraduated(event.currentTarget.name)
                   }}
                   />
                   <label className="form-check-label">Graduated</label>
                   <button type="submit" className="btn btn-primary" style={{ marginTop: '10px' }} onClick={()=>{this.setState({update:true})
                  // this.state.updateCID = student.cid
                  this.studentObj= student
                  // console.log("aslkdjaks")
                  // console.log(this.studentObj)

                  }} >Alter</button>

                </li>
              )
            })
          }
        </ul>
        <p>
          Total student:{
            this.state.studentCount
          }
        </p>
        {/* <StudentList/> */}
      </div>
    );
  }
}
export default App;