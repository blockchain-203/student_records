const MarkList = artifacts.require('MarkList')
const SubjectList = artifacts.require('SubjectList')
const StudentList = artifacts.require('StudentList')
contract('MarkList', (account) => {
    beforeEach(async () => {                
        this.marklist = await MarkList.deployed()
 
    })
    // assert.equal(MarkList._json.contractName, "MarkList", )

    it('testing three name of cotract', async () => {
        markcontractname = MarkList._json.contractName;
        assert.equal(markcontractname, "MarkList")
        subjectcontractname = SubjectList._json.contractName;
        assert.equal(subjectcontractname, "SubjectList")
        studentcontractname = StudentList._json.contractName;
        assert.equal(studentcontractname, "StudentList")
    })
    
    it('deploys successfully', async () => {   
        const address = await this.marklist.address
        isValidAddress(address)
    })
    it('adding student', async() => {
        return StudentList.deployed().then(async(instance) => {
            s = instance;
            let names = ["karma Tshewang","dendup Tshering","jampel dorji","dorji phuntsho","dechen namgay"];
            studentCid = 1;
            for (let i = 0; i < names.length; i++) {
                s.createStudent(studentCid, names[i]);
                studentCid ++;        
            }
        })
    })
    it('adding subject', async() => {
        return SubjectList.deployed().then(async(instance) =>{
            s = instance;
            let subjects = ["operting system","ACT","Economic","Discrete math","programming i math"];
            let codes = ["CB203","ACF102","ECO103","MAT101","MAT102"];
            id = 1;
            for (let i = 0; i < subjects.length; i++) {
               s.createSubject(codes[i],subjects[i]);
               id ++;
                
            }

        })
    })
    it('test adding marks', async () => {
        return MarkList.deployed().then (async(instance) => {
            s = instance;
            return s.addMarks(1, "csb203", 1).then (async(transaction) => {
            return s.marksCount().then(async(count) => {
                assert.equal(count,1)
                return s.findMarks(1, "csb203").then(async(marks) => {
                    assert.equal(marks.code,"csb203")
                    assert.equal(marks.grades, 1)
                })
            })
            })
        })
    })
    it('test updating marks', async() => {
        return MarkList.deployed().then (async(instance) => {
            s = instance;
            return s.findMarks(1, "csb203").then(async(omarks) => {
                assert.equal(omarks.code,"csb203")
                assert.equal(omarks.cid, 1)
                assert.equal(omarks.grades, 1)
            s.updateMarks(1,"csb203", 3);
            return s.findMarks(1,"csb203").then(async(nmarks) =>{
                assert.equal(nmarks.code, "csb203")
                assert.equal(nmarks.cid,1 )
                assert.equal(nmarks.grades, 1)
            })
                
            });
            
        })
    })
    
})
function isValidAddress(address){
    assert.notEqual(address, 0x0) // assert.notEqual is also function that takes parameter acutal and expected values
    assert.notEqual(address,'')
    assert.notEqual(address,null)
    assert.notEqual(address,undefined)
}
